package database;

public abstract class DatabaseModule {

	private DatabaseConnectionHandler connectionhandler;

	public DatabaseConnectionHandler getConnectionhandler() {
		return connectionhandler;
	}

	public void setConnectionhandler(DatabaseConnectionHandler connectionhandler) {
		this.connectionhandler = connectionhandler;
	}
}
