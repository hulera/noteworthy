package database;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

public class DatabaseLogFileWriter {

	public static final String logFileName = "databaseChangeLog.txt";
	
	private String logFileDir;
	
	private DatabaseCreator creator;
	
	public DatabaseLogFileWriter(String dir) {
		setLogFileDir(dir);
	}
	
	public void doDatabaseSetup() {
		try {
			if(!creator.isSetup()) {
				System.out.println("Datenbank erzeugen...");
				try {
					creator.deleteDatabaseExisting();
				} catch (Exception e) {
					e.printStackTrace();
				}
				File logFile = getFile();
				FileWriter appender = new FileWriter(logFile.getCanonicalPath(), true);
				System.out.println(logFile.getCanonicalPath());
				appender.write("\n" + "Updating Database!\n");
				appender.write(LocalDateTime.now().toString() + "\n");
				appender.write(creator.getSQLFileFingerprint().toString() + "\n");
				creator.createDatabase();
				appender.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public File getFile() throws IOException {
		File file = new File(logFileDir + "/" + logFileName);
		
		if(!file.exists())
			file.createNewFile();
		
		return file;
	}

	public String getLogFileDir() {
		return logFileDir;
	}

	public void setLogFileDir(String logFileDir) {
		this.logFileDir = logFileDir;
	}

	public void addDataBaseCreator(DatabaseCreator creator) {
		this.creator = creator;
	}
}
