package database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class DATABASE {

	// Die folgenden drei Zeilen m�ssen vorher in mysql ausgef�hrt werden!
	/**
	 * CREATE USER 'userName'@'localhost' IDENTIFIED BY 'userPassword';
	 * GRANT ALL PRIVILEGES ON * . * TO 'username'@'localhost';
	 * FLUSH PRIVILEGES;
	 */
	
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://localhost";
	public static Connection CONNECTION;

	public static final String USER = "bpuser";
	public static String PASS;// = "bppassword";
	public static final String NAME = "airport";

	public static void connect() throws Exception {
		Scanner scanner = new Scanner(new File("/home/nicholas/Uni/BP/gitDir/yy45siwe-airport/AirportWeb/keys.txt"));
		PASS = scanner.nextLine();
		Class.forName(DATABASE.JDBC_DRIVER);
		CONNECTION = DriverManager.getConnection(DATABASE.DB_URL, DATABASE.USER, DATABASE.PASS);
		selectDatabase();
	}

	private static void selectDatabase() throws Exception {
		Statement stmt = DATABASE.CONNECTION.createStatement();
		String sql = "SHOW DATABASES LIKE '" + DATABASE.NAME + "'";
		ResultSet rs = stmt.executeQuery(sql);

		if (!rs.next()) {
			System.out.println("Database doesn't exist and will be created");
			createDatabase();
			System.out.println("Database successfully created");
		}
		else {
			Statement stmt2 = DATABASE.CONNECTION.createStatement();
			String sql2 = "USE airport";
			ResultSet rs2 = stmt2.executeQuery(sql2);
			rs2.close();
			stmt2.close();
			System.out.println("Database connection was established!");
		}

		rs.close();
		stmt.close();
	}

	private static void createDatabase() throws Exception {
		
		Statement stmt1 = DATABASE.CONNECTION.createStatement();
		String sql1 = "CREATE DATABASE " + NAME;
		stmt1.executeUpdate(sql1);
		stmt1.close();
		
		Statement stmt2 = DATABASE.CONNECTION.createStatement();
		String sql2 = "USE " + NAME;
		stmt2.executeUpdate(sql2);
		stmt2.close();
		
		ScriptRunner sr = new ScriptRunner(CONNECTION, false, true);
		sr.runScript(new BufferedReader(new FileReader("res/airport.sql")));
	}

	public static void disconnect() throws Exception {
		CONNECTION.close();
	}
}
