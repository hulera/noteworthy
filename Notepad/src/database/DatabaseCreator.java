package database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.mysql.jdbc.Connection;

public class DatabaseCreator extends DatabaseModule {

	private DatabaseConnectionHandler databaseConnection;

	private String _sqlFile;

	public DatabaseCreator(DatabaseConnectionHandler databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	public void createDatabase() throws Exception {
		ScriptRunner sr = new ScriptRunner(databaseConnection.connection, false, true);
		sr.runScript(new BufferedReader(new FileReader(_sqlFile)));
	}

	public void clearDatabase() throws SQLException, IOException {

		List<String> tableNames = getAllTablenames(_sqlFile);

		for (String name : tableNames) {
			Statement stmt = databaseConnection.connection.createStatement();
			String nameOfExercises = "DELETE FROM " + name + ";";
			stmt.execute(nameOfExercises);
		}
	}

	public void deleteDatabaseFile() throws Exception {
		Statement stmt2;

		List<String> tableNames = getAllTablenames(_sqlFile);

		for (int i = tableNames.size() - 1; i >= 0; i--) {
			stmt2 = databaseConnection.connection.createStatement();
			stmt2.executeUpdate("DROP TABLE " + tableNames.get(i));
			stmt2.close();
		}

	}

	public void deleteDatabaseExisting() throws Exception {
		Statement stmt2;
		List<String> existingNames = getExistingTableNames();
		System.out.println(existingNames);

			while (existingNames.size() > 0) {
				stmt2 = databaseConnection.connection.createStatement();
				String name = existingNames.get((int) (Math.random() * existingNames.size()));
				try {
					stmt2.executeUpdate("DROP TABLE " + name);
					existingNames.remove(name);
				} catch (Exception e) {
					if(!e.getMessage().contains("key constraint"))//a foraign key is ok
						throw e;
				}

			}
	}

	private List<String> getExistingTableNames() throws SQLException {
		ResultSet existingTableNames = databaseConnection.connection.createStatement().executeQuery("SHOW TABLES;");

		List<String> akku = new LinkedList<>();
		while (existingTableNames.next())
			akku.add(existingTableNames.getString(1));

		return akku;
	}

	public boolean isSetupX() throws SQLException, IOException {
		List<String> sqlFileNames = DatabaseCreator.getAllTablenames(_sqlFile);

		DatabaseMetaData md = databaseConnection.connection.getMetaData();

		String[] types = { "TABLE" };
		ResultSet rs = md.getTables(databaseConnection.getDbName(), null, "%", types);

		List<String> existing = new LinkedList<String>();

		while (rs.next())
			existing.add(rs.getString("TABLE_NAME"));

		existing.sort(new Comparator<String>() {

			@Override
			public int compare(String arg0, String arg1) {
				return arg0.compareTo(arg1);
			}
		});

		sqlFileNames.sort(new Comparator<String>() {

			@Override
			public int compare(String arg0, String arg1) {
				return arg0.compareTo(arg1);
			}
		});

		return existing.equals(sqlFileNames);
	}

	public static List<String> getAllTablenames(String _sqlFile) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(_sqlFile));

		int start = "CREATE TABLE `".length();

		List<String> result = reader.lines().filter(s -> s.contains("CREATE TABLE `"))
				.map(s -> s.substring(start, s.lastIndexOf("`"))).collect(Collectors.toList());

		reader.close();

		return result;
	}

	public String get_sqlFile() {
		return _sqlFile;
	}

	public void set_sqlFile(String _sqlFile) {
		this._sqlFile = _sqlFile;
	}

	public boolean isSetup() throws IOException, SQLException {
		List<String> akku = new LinkedList<>();

		akku = getSQLFileFingerprint();

		List<String> akku2 = new LinkedList<>();

		for (String table : getExistingTableNames()) {
			akku2.add(table);
			ResultSet rsColumns = databaseConnection.connection.createStatement()
					.executeQuery("SELECT * FROM " + table + ";");

			
			for(int i = 0;i < rsColumns.getMetaData().getColumnCount();i++) {
				akku2.add(rsColumns.getMetaData().getColumnName(i + 1));
				akku2.add(rsColumns.getMetaData().getColumnTypeName(i + 1));
			}
		}
		
		akku.sort(new Comparator<String>() {

			@Override
			public int compare(String arg0, String arg1) {
				return arg0.compareTo(arg1);
			}
		});
		akku2.sort(new Comparator<String>() {

			@Override
			public int compare(String arg0, String arg1) {
				return arg0.compareTo(arg1);
			}
		});
		return akku.equals(akku2);
	}

	public List<String> getSQLFileFingerprint() throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(_sqlFile));

		
		boolean addTo = false; 
		
		List<String> akku = new LinkedList<>();

		while (reader.ready()) {
			String line = reader.readLine();

			if (line.startsWith(");") || line.contains("FOREIGN"))
				addTo = false;
			else if (line.startsWith("CREATE TABLE")) {
				addTo = true;

				akku.add(line.substring("CREATE TABLE `".length(), line.indexOf("(") - 2));

			} else if (addTo) {// within a table declaration -- retrieving attributes and there types

				int beginName = line.indexOf("`");
				int endName = line.indexOf("`", beginName + 1);
				akku.add(line.substring(beginName + 1, endName));

				int base = endName + 3;
				char[] terminationChars = { ' ', ',', '(', '\n' };// depending on the position the type-identifier can
																	// end with any of these characters

				int termIndex = 0;

				int endTypeIndex = Integer.MAX_VALUE;
				for (char c : terminationChars) {
					termIndex = line.indexOf(c, base);
					if (termIndex >= 0)
						endTypeIndex = Math.min(endTypeIndex, termIndex);
				}

				if (endTypeIndex == Integer.MAX_VALUE)
					endTypeIndex = line.length();

				akku.add(line.substring(endName + 1, endTypeIndex).trim());
			}
		}

		reader.close();
		return akku;
	}
}