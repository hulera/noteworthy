package database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.util.Random;

public class DatabaseConnectionHandler {

	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

	public static final String DB_URL = "jdbc:mysql://localhost";
	public Connection connection;

	private String user = "bpDummy";
	private String pass = "bpDummy";
	private String dbName = "dummy";

	private String file;

	public DatabaseConnectionHandler(String accessFile) throws Exception {
		this.file = accessFile;

		FileReader fr = new FileReader(accessFile);
		BufferedReader br = new BufferedReader(fr);

		String userLine = br.readLine();
		String passwordLine = br.readLine();
		String dbNameLine = br.readLine();

		String userString = "user";
		String passwordString = "password";
		String dbNameString = "dbName";

		if (userLine.startsWith(userString)) {
			user = userLine.substring(0 + userString.length() + 1);
		} else {
			br.close();
			throw new IllegalArgumentException(userLine + "line isn't correct");

		}

		if (passwordLine.startsWith(passwordString))
			pass = passwordLine.substring(0 + passwordString.length() + 1);
		else {
			br.close();
			throw new IllegalArgumentException(passwordLine + "line isn't correct");
		}

		if (dbNameLine.startsWith(dbNameString))
			dbName = dbNameLine.substring(0 + dbNameString.length() + 1);
		else {
			br.close();
			throw new IllegalArgumentException(dbNameLine + "line isn't correct");
		}

		br.close();
		connect();
	}

	/**
	 * Trys to connect to the databaseserver/instance if not possible an Exception
	 * is thrown NOTE: Must be once called before any other method i called.
	 * 
	 * @throws Exception
	 */

	public void connect() throws Exception {
		Class.forName(JDBC_DRIVER);
		connection = DriverManager.getConnection(DB_URL, user, pass);
		selectDatabase();
	}

	private void selectDatabase() throws Exception {
		Statement stmt = connection.createStatement();
		String use = "USE " + dbName + ";";
		stmt.executeQuery(use);
	}

	public Connection getConnection() {
		return connection;
	}

	public void disconnect() throws Exception {
		connection.close();
	}

	public String getDbName() {
		return dbName;
	}
}