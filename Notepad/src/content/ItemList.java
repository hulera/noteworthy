package content;

import java.util.LinkedList;
import java.util.List;

public class ItemList<T> {
	
	private String title;
	
	private List<T> items;
	
	public ItemList(String title) {
		this.setTitle(title);
		items = new LinkedList<T>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<T> getItems() {
		return items;
	}

	public void addItems(List<T> items) {
		this.items.addAll(items);
	}
	
	public T removeItem(int index) {
		return items.remove(index);
	}
	
	public boolean remove(T item) {
		return items.remove(item);
	}
	
	public void addItem(T item) {
		items.add(item);
	}
	
	public void addItem(int index, T item) {
		items.add(index,item);
	}
	
	@Override
	public String toString() {
		
		return title + ";" + this.items.toString();
	}
}