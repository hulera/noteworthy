package content.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import content.ItemList;
import content.exceptions.AlreadyAnEntryWithTitle;
import database.DatabaseModule;

public class ItemListDatabaseModule extends DatabaseModule {
	
	public void storeNewItemList(ItemList<String> itemList) throws SQLException, AlreadyAnEntryWithTitle {
		
		Statement existingLists = getConnectionhandler().getConnection().createStatement();
		ResultSet alreadyPresent = existingLists.executeQuery("SELECT * FROM ItemList WHERE title = '" + itemList.getTitle() + "';");
		
		if(alreadyPresent.first())//checking if empty
			throw new AlreadyAnEntryWithTitle("There is already an ItemList with title:" +  itemList.getTitle());
			
		Statement mainStatement = getConnectionhandler().getConnection().createStatement();
		StringBuffer insertAkku = new StringBuffer();
		insertAkku.append("INSERT INTO ItemList (title,creationDate,file) ");
		insertAkku.append("VALUES ('");
		insertAkku.append(itemList.getTitle());
		insertAkku.append("',");
		insertAkku.append("''");// TODO Create dateclass
		insertAkku.append(",");
		insertAkku.append("'_Not_Set_Yet'");
		insertAkku.append(");");
		mainStatement.execute(insertAkku.toString());

		int id = getItemListID(itemList.getTitle());

		storeItems(itemList, id);
	}

	private void storeItems(ItemList<String> itemList, int id) throws SQLException {
		String base = "INSERT INTO Item(iT_ID,text) VALUES (" + id + ",'";

		Statement linesStatements;
		for (String item : itemList.getItems()) {
			linesStatements = getConnectionhandler().getConnection().createStatement();
			linesStatements.execute(base + item + "');");
		}
	}

	public ItemList<String> getItemList(String name) throws SQLException {

		if (!getItemListNames().contains(name))
			return null;

		ItemList<String> result = new ItemList<String>(name);

		Statement itemSearch = getConnectionhandler().getConnection().createStatement();
		String query = "SELECT Item.text FROM ItemList,Item WHERE Item.iT_ID = ItemList.ID AND title = '" + name + "';";
		ResultSet queryResult = itemSearch.executeQuery(query);
	
		while (queryResult.next())
			result.addItem(queryResult.getString("Item.text"));

		return result;
	}

	public List<String> getItemListNames() throws SQLException {
		List<String> akku = new LinkedList<String>();

		Statement allName = getConnectionhandler().getConnection().createStatement();
		ResultSet names = null;
		try {
			names = allName.executeQuery("SELECT title FROM ItemList;");
		} catch (Exception e) {
			e.printStackTrace();
		}
		while (names.next())
			akku.add(names.getString("title"));

//		System.out.println(akku);

		return akku;
	}

	public void update(String itemListName, ItemList<String> newValue) throws SQLException {
		removeAllItems(itemListName);
		
		int id = getItemListID(itemListName);
		
		StringBuffer updateString = new StringBuffer("UPDATE ItemList SET ");
		updateString.append("title = '");
		updateString.append(newValue.getTitle());
		updateString.append("', ");
		updateString.append("creationDate = '");
		updateString.append("");
		updateString.append("',");
		updateString.append("file ='");
		updateString.append("");
		updateString.append("'");
		updateString.append("WHERE ID = ");
		updateString.append(id);
		updateString.append(";");
		
		Statement updateStatement = getConnectionhandler().getConnection().createStatement();
		updateStatement.execute(updateString.toString());
		
		
		storeItems(newValue, id);		
		
	}
	
	public void removeAllItems(String itemListTitle) throws SQLException {
		int id = getItemListID(itemListTitle);
		
		if(id != -1) {
			Statement deleteStatement = getConnectionhandler().getConnection().createStatement();
			deleteStatement.execute("DELETE FROM Item WHERE iT_ID = " + id + ";");
		}
	}

	private int getItemListID(String itemListTitle) throws SQLException {
		Statement idSearchStatement = getConnectionhandler().getConnection().createStatement();
		ResultSet rs = idSearchStatement.executeQuery("SELECT * FROM ItemList WHERE title = '" + itemListTitle + "';");
		
		return getId(rs);
	}
	
	private int getId(ResultSet rs) throws SQLException {
		int id = -1;
		
		if(rs.next())
			id = rs.getInt("ID");
	
		return id;
	}

}