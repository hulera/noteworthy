package listener;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import content.database.ItemListDatabaseModule;
import database.DatabaseConnectionHandler;
import database.DatabaseCreator;
import database.DatabaseLogFileWriter;
import servlet.duty.ServletDutyFactory;

/**
 * Application Lifecycle Listener implementation class AirportContextListener
 *
 */
@WebListener
public class NotepadContextListener implements ServletContextListener {

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent event) {

		ServletContext sc = event.getServletContext();
		DatabaseConnectionHandler db = null;
		try {
			File f = new File(sc.getRealPath("/") + "resourcess");
			for (int i = 0; i < f.list().length; i++)
				System.out.println(f.list()[i]);
			db = new DatabaseConnectionHandler(sc.getRealPath("/") + "resourcess/dbLoginData.txt");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			db.connect();
			System.out.println("SQL Server Connection successfull");
		} catch (Exception e) {
			System.out.println("Connecting to mySQL Database failed. Is SQL Server running?");
			e.printStackTrace();
		}
		sc.setAttribute("databaseConnectionHandler", db);
		DatabaseCreator creator = new DatabaseCreator(db);
		creator.set_sqlFile(sc.getRealPath("/") + "resourcess/notepad.sql");
		
		DatabaseLogFileWriter logFileWriter = new DatabaseLogFileWriter(sc.getRealPath("/"));
		
		logFileWriter.addDataBaseCreator(creator);
	
		logFileWriter.doDatabaseSetup();
		
		ItemListDatabaseModule ildm = new ItemListDatabaseModule();
		ildm.setConnectionhandler(db);
		sc.setAttribute("ItemListDatabaseModule",ildm);
		
		sc.setAttribute("ServletDutyFactory", new ServletDutyFactory());
		
		System.out.println("I am NotePadContextListener.");
		
		
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent event) {
		try {
			ServletContext sc = event.getServletContext();
			DatabaseConnectionHandler dch = (DatabaseConnectionHandler) sc.getAttribute("databaseConnectionHandler");
			dch.disconnect();
		} catch (Exception e) {
			System.out.println("Disconnecting from mySQL Database failed.");
			e.printStackTrace();
		}
		System.out.println("Distruction");
	}

}
