package util;

public interface DateHandler<T> {

	public T today();
	
	public void setFormat(String formater);
	
	public String todayAsString();
	
	public String convertToString(T date);
	
	public T convertToT(String date);
	
}
