package servlet.duty;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import content.ItemList;
import content.database.ItemListDatabaseModule;
import content.exceptions.AlreadyAnEntryWithTitle;

public class StoreItemlistDuty extends ServletDuty {

	public StoreItemlistDuty() {
		super("StoreItemList");
	}

	@Override
	public void job(HttpServletRequest request, ServletContext servletContext) {

		ItemListDatabaseModule ildm = (ItemListDatabaseModule) servletContext
				.getAttribute("ItemListDatabaseModule");

		String title = request.getParameter("titleArea");
		String text = request.getParameter("mainArea");
		ItemList<String> iList = new ItemList<String>(title);
		String[] lines = text.split(System.lineSeparator());
		for (String line : lines)
			iList.addItem(line);

		try {
			ildm.storeNewItemList(iList);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (AlreadyAnEntryWithTitle e) {
			e.printStackTrace();
		}
	}
}