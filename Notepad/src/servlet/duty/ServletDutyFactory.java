package servlet.duty;

import java.util.LinkedList;
import java.util.List;

public class ServletDutyFactory {
	static List<ServletDuty> dutys;
	static {
		dutys  = new LinkedList<>();
		dutys.add(new StoreItemlistDuty());
		dutys.add(new UpdateItemlistDuty());
		dutys.add(new LoadItemlistDuty());
	}
	
	public ServletDuty getDutyFor(String jobName) {
		for(ServletDuty currentDuty : dutys)
			if(currentDuty.JobName.equals(jobName))
				return currentDuty;
			
		return null;
	}

}
