package servlet.duty;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public abstract class ServletDuty {
	
	public final String JobName;
	
	
	public ServletDuty(String jobName) {
		this.JobName = jobName;
	}
	 
	public abstract void job(HttpServletRequest request, ServletContext servletContext);

}
