<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page import="java.util.*"%>
    

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Notizen...</title>

</head>

<body>



 <form method="post" action="inD">
		
	<h3>
		<p class="title">Notizen:</p>
	</h3>

		<select name="listNames" size="1" id="listNames">
			<c:forEach items="${namesList}" var="name">
				<option>${name}</option>
			</c:forEach>
		
		</select>

		<button type="submit" name="load" id="load">Laden</button>
		Aktueller Notizblock: <c:out value="${loadedTitle }" default="Kein Notizbuch ausgewählt" />
		<br>
		<button type="submit" name="freshField" id="freshField">Neues leeres Feld</button>
		<br>
		<br>
		<input rows="1" cols="40" name="titleArea" id="titleArea" value="${loadedTitle }"/>
		<br>
		<textarea rows="20" cols="80" name="mainArea" id="mainArea">${loadedContent}</textarea>
		<br>
		<button type="submit" name="save" id="save">Speichern</button>
		<button type="submit" name="update" id="update">Änderungen speichern</button>
		<br>
		

	</form>

</body>
</html>