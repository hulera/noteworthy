package testpackage;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import database.DatabaseLogFileWriter;

class TestChangelog {
	
	static String testFilePathUpdate = "src/test/resource/TestUpdate.dbe";
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		FileWriter fw = new FileWriter(testFilePathUpdate);
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write("hello");
		bw.newLine();
		bw.write("number 2");
		bw.newLine();
		bw.write("bye");
		
		bw.close();
		
	}

	@Test
	void test() {
		DatabaseLogFileWriter lgw = new DatabaseLogFileWriter(testFilePathUpdate);
	}

}
