package util;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;

import database.UpdateRepresentation;

public class DatabaseLogWriter {
	private LogFileWriter logWriter;

	public DatabaseLogWriter(String directory, String fileName, String fileExtension) throws IOException {
		logWriter = new LogFileWriter(directory, fileName, fileExtension);
		logWriter.createFile();
	}

	
	public static void main(String[] args) throws IOException {
		DatabaseLogWriter x = new DatabaseLogWriter("/home/nicholas","dummy", "log");
		
		x.appendEntry(new UpdateRepresentation(new File("/home/nicholas/git/noteworthy/MavenNotepad/src/main/resources/resources/database/versioning_v0_0_0.dbe")));
	}
	public void appendEntry(UpdateRepresentation update) throws IOException {
		logWriter.appendLine("UPDATE");
		
		logWriter.appendLine(getTimestamp());
		
		logWriter.appendLine(update.getName());
		
		logWriter.appendLine(Arrays.toString(update.getVersionVektor()));
		
		for (String comment : update.getComments())
			logWriter.appendLine(comment);

		logWriter.appendLine("---");
		
		for (String instruction : update.getInstructions())
			logWriter.appendLine(instruction);
		
		logWriter.appendLine("ENDUPDATE");
		logWriter.appendLine("");
	}

	private String getTimestamp() {
		return LocalDateTime.now().toString();
	}

}