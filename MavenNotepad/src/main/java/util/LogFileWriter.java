package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LogFileWriter {
	
	private String dir;
	private String fileName;
	private String fileExtension;
	
	private File file;
	
	public LogFileWriter(String directory, String fileName, String fileExtension) throws IOException {
		this.setDir(directory);
		this.setFileName(fileName);
		this.setFileExtension(fileExtension);
	}
	
	public void createFile() throws IOException {
		file = new File(dir + "/" + fileName + "." + fileExtension);
		if(!file.exists())
			file.createNewFile();
	}

	public void appendLine(String line) throws IOException {
		FileWriter writer = new FileWriter(file,true);
		writer.append(line);
		writer.append(System.lineSeparator());
		writer.close();
	}
	
	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
}