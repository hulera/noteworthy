package servlet.duty;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import content.ItemList;
import content.database.ItemListDatabaseModule;

public class UpdateItemlistDuty extends ServletDuty {

	public UpdateItemlistDuty() {
		super("UpdateItemList");
	}

	@Override
	public void job(HttpServletRequest request, ServletContext servletContext) {
		ItemListDatabaseModule ildm = (ItemListDatabaseModule) servletContext
				.getAttribute("ItemListDatabaseModule");


		String newTitle = request.getParameter("titleArea");
		String newText = request.getParameter("mainArea");
		String oldTitle = request.getParameter("origTitle");
		
		ItemList<String> iList = new ItemList<String>(newTitle);
		String[] lines = newText.split(System.lineSeparator());
		for (String line : lines)
			iList.addItem(line);

		try {
			ildm.update(oldTitle, iList);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
