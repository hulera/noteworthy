package servlet.duty;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import content.ItemList;
import content.database.ItemListDatabaseModule;

public class LoadItemlistDuty extends ServletDuty {

	public LoadItemlistDuty() {
		super("LoadItemList");
	}

	@Override
	public void job(HttpServletRequest request, ServletContext servletContext) {
		ItemListDatabaseModule ildm = (ItemListDatabaseModule) servletContext
				.getAttribute("ItemListDatabaseModule");
		
		String toLoad = request.getParameter("listNames");
		
		ItemList<String> loaded = null;
		try {
			loaded = ildm.getItemList(toLoad);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("loadedTitle",loaded.getTitle());
		
		StringBuffer sb = new StringBuffer();
		for(String line : loaded.getItems()) {
			System.out.println(line);
			sb.append(line);
			sb.append(System.lineSeparator());
		}
		
		request.setAttribute("loadedContent", sb.toString());
		
		
	}
}