package listener;

import java.time.LocalDate;
import java.util.LinkedList;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import database.DatabaseConnection;

/**
 * Application Lifecycle Listener implementation class NotepadSessionListener
 *
 */
@WebListener
public class NotepadSessionListener implements HttpSessionListener {

    /**
     * Default constructor. 
     */
    public NotepadSessionListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent event)  { 
		/********************************************
		 * if the created session is the first active session, then create a connection to SQL Server
		 ********************************************/
    	ServletContext ctx = event.getSession().getServletContext();
    	int sessionCounter;
		synchronized (ctx) {
			sessionCounter = (int) ctx.getAttribute("sessionCounter");
			if (sessionCounter < 1) {
				
				try {
					((DatabaseConnection) ctx.getAttribute("databaseConnection")).connect();
					System.out.println(LocalDate.now().toString() + ": SQL Server Connection successfully created.");
					ctx.log(LocalDate.now().toString() + ": SQL Server Connection successfully created.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			sessionCounter++;
			System.out.println("Active Sessions: " + sessionCounter);
			ctx.setAttribute("sessionCounter", sessionCounter);
		}
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent event)  { 
    	/********************************************
		 * If the last active Session is destroyed, disconnect the SQL Connection.
		 ********************************************/
    	ServletContext ctx = event.getSession().getServletContext();
    	int sessionCounter;
    	synchronized (ctx) {
    		sessionCounter = (int) ctx.getAttribute("sessionCounter");
    		sessionCounter--;
    		
    		//sessionCounter darf niemals kleiner 0 sein.
    		if (sessionCounter < 0)
				sessionCounter = 0;
    		
    		ctx.setAttribute("sessionCounter", sessionCounter);
    		
    		if (sessionCounter < 1) {
    			
    			try {
					((DatabaseConnection) ctx.getAttribute("databaseConnection")).disconnect();
					ctx.log(LocalDate.now().toString() + ": SQL Server Connection successfully disconnected.");
				} catch (Exception e) {
					e.printStackTrace();
				}
    		}
    	}
    }
	
}
