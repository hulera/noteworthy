package listener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import content.database.ItemListDatabaseModule;
import database.DatabaseConnection;
//import database.DatabaseConnectionHandler;
//import database.DatabaseCreator;
import database.DatabaseLogFileWriter;
import database.DatabaseModule;
import database.DatabaseVersionManager;
import servlet.duty.ServletDutyFactory;

/**
 * Application Lifecycle Listener implementation class AirportContextListener
 *
 */
@WebListener
public class NotepadContextListener implements ServletContextListener {

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent event) {

		ServletContext sc = event.getServletContext();

		
		String dbePath = sc.getRealPath("/WEB-INF/classes/resources/database");
		String connectionPath = sc.getRealPath("/WEB-INF/classes/resources/dbLoginData.txt");
		
		DatabaseConnection standardCon = new DatabaseConnection(connectionPath);
		DatabaseVersionManager dvm = new DatabaseVersionManager(dbePath, standardCon);
		dvm.addDB("notepad");
			try {
				dvm.setupDatabasesNEW();
			} catch (Exception e) {
				e.printStackTrace();
			}
		sc.setAttribute("DatabaseVersionManager", dvm);
		sc.setAttribute("databaseConnection", standardCon);
		
		
		DatabaseLogFileWriter lfw = new DatabaseLogFileWriter();
		sc.setAttribute("dbLogger", lfw);
		
		
		List<DatabaseModule> dbModules = new LinkedList<DatabaseModule>();
		dbModules.add(new ItemListDatabaseModule());
		sc.setAttribute("dbModules", dbModules);
	
		
		int sessionCounter = 0;		
		sc.setAttribute("sessionCounter", sessionCounter);
		
		
		System.out.println("I am NotePadContextListener.");
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent event) {
		
	}

}
