package content.exceptions;

public class AlreadyAnEntryWithTitle extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5122314307518388554L;

	public AlreadyAnEntryWithTitle(String message) {
		super(message);
	}
}
