package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import content.ItemList;
import content.database.ItemListDatabaseModule;
import servlet.duty.LoadItemlistDuty;
import servlet.duty.ServletDuty;
import servlet.duty.ServletDutyFactory;
import servlet.duty.StoreItemlistDuty;
import servlet.duty.UpdateItemlistDuty;
import util.Pair;

/**
 * Servlet implementation class VarListController
 */
@WebServlet("/inD")
public class SimplePad extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static List<Pair<String, String>> buttonDutyAssociation;
	static {
		buttonDutyAssociation = new LinkedList<>();
		buttonDutyAssociation.add(new Pair<String, String>("save", new StoreItemlistDuty().JobName));
		buttonDutyAssociation.add(new Pair<String, String>("update", new UpdateItemlistDuty().JobName));
		buttonDutyAssociation.add(new Pair<String, String>("load", new LoadItemlistDuty().JobName));
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher view = request.getRequestDispatcher("SimplePad.jsp");
		ItemListDatabaseModule ildm = (ItemListDatabaseModule) getServletContext()
				.getAttribute("ItemListDatabaseModule");

		for (Pair<String, String> possibleDuty : buttonDutyAssociation)
			if ("".equals(request.getParameter(possibleDuty.getFirst()))) {
				ServletDutyFactory dutyFactory = (ServletDutyFactory) getServletContext()
						.getAttribute("ServletDutyFactory");
				ServletDuty correspondingDuty = dutyFactory.getDutyFor(possibleDuty.getSecond());
				correspondingDuty.job(request, getServletContext());
			}

		try {
			request.setAttribute("namesList", ildm.getItemListNames());
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("asdasdasd");
		view.forward(request, response);

	}

}
