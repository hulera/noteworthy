package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DatabaseVersionManager {

	private DatabaseConnection connection;

	private String baseFolder;

	private static DatabaseSelector versionDB = new DatabaseSelector("versioning");

	private UpdateRepresentation version000;

	private static List<String> generalToUpdate;

	static {
		generalToUpdate = new LinkedList<>();

		generalToUpdate.add("usermanagment");
	}

	private List<String> dbsToUpdate;

	/**
	 * Basic Constructor for this class
	 * 
	 * @param baseFolder
	 *            -- Location where updates are searched for
	 * @param connectionToUse
	 */
	public DatabaseVersionManager(String baseFolder, DatabaseConnection connectionToUse) {
		connection = connectionToUse;
		this.baseFolder = baseFolder;

		try {
			version000 = new UpdateRepresentation(new File(baseFolder + "/versioning_v0_0_0.dbe"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		dbsToUpdate = new LinkedList<String>();
	}

	public void addDB(String name) {
		dbsToUpdate.add(name);
	}

	public static void main(String[] args) throws Exception {
		DatabaseVersionManager dvm = new DatabaseVersionManager(
				"/home/nicholas/git/noteworthy/MavenNotepad/src/main/resources/database", new DatabaseConnection(
						"/home/nicholas/git/noteworthy/MavenNotepad/src/main/webapp/resourcess/dbLoginData.txt"));
		dvm.addDB("notepad");
		dvm.addDB("notepadX");
		dvm.setupDatabasesNEW();

	}

	public void setupDatabasesNEW() throws FileNotFoundException, IOException, SQLException, Exception {

		connection.connect();

		// setting up versionmanagment
		if (!versionDBIsPresent())
			createVersionDB();

		if (!versionTablesIsPresent())
			initVersionTable();

		excecuteWaitingUpdatesFor(version000.getName());
		
		for (String name : generalToUpdate)
			createDB(name);

		// always update- Some dbs/tables do not belong to a plugin nor to 
		// the versionmanagment 
		updateAll(generalToUpdate);

		for (String name : dbsToUpdate)
			createDB(name);

		// plugins installed on Server
		updateAll(dbsToUpdate);

		connection.disconnect();
	}

	private void updateAll(List<String> nameList) throws Exception {
		for (String name : nameList) {
			excecuteWaitingUpdatesFor(name);
		}
	}

	private void excecuteWaitingUpdatesFor(String name) throws Exception {
		List<UpdateRepresentation> waitingUpdates = getAllWaitingUpdatesFor(name);
		for (UpdateRepresentation update : waitingUpdates) {

			executeUpdate(update);
		}
	}

	private void createVersionDB() throws SQLException {
		Statement createDBSTMT = connection.getConnection().createStatement();
		createDBSTMT.execute("CREATE DATABASE " + versionDB.getDatabaseName() + ";");
	}

	private void createDB(String name) throws SQLException {

		Statement createDBSTMT = connection.getConnection().createStatement();
		try {
			createDBSTMT.execute("CREATE DATABASE " + name + ";");
		} catch (Exception e) {

		}

	}

	private boolean versionDBIsPresent() throws Exception {
		try {
			connection.selectDatabase(versionDB);
		} catch (Exception e) {
			if (e.getMessage().contains("Unknown database '" + versionDB.getDatabaseName() + "'")) {
				return false;
			} else
				throw e;
		}
		return true;
	}

	private boolean versionTablesIsPresent() throws Exception {
		connection.selectDatabase(versionDB);

		Statement check = connection.getConnection().createStatement();
		ResultSet rs = null;
		rs = check.executeQuery("SHOW TABLES;");
		boolean result = false;

		String firstTable = version000.getCreateTableNames().get(0);		//versioning_v0_0_0.dbe is set stable as it is with at least one table in it
		while (rs.next()) {
			if (rs.getString(1).equalsIgnoreCase(firstTable))
				result = true;
		}
		return result;
	}

	private void initVersionTable() throws FileNotFoundException, IOException, SQLException, Exception {
		executeUpdate(version000);

	}


	public int[] getLatesVersionFor(String name) throws Exception {

		connection.selectDatabase(versionDB);
		
		Statement askCurrentVersion = connection.getConnection().createStatement();

		List<String> tableNames = version000.getCreateTableNames();

		ResultSet result = askCurrentVersion
				.executeQuery("SELECT * FROM " + tableNames.get(0) + " WHERE name = '" + name + "' ORDER BY ID DESC;");

		int[] res = { -1, -1, -1 };

		if (result.next()) {
			res[0] = result.getInt(3);
			res[1] = result.getInt(4);
			res[2] = result.getInt(5);
		}
		return res;
	}

	public int[] getLatesUpdateVersion() throws FileNotFoundException, IOException {
		List<UpdateRepresentation> relevant = getAllUpdates();
		java.util.Collections.sort(relevant);

		UpdateRepresentation mostRecent = relevant.get(relevant.size() - 1);

		return mostRecent.getVersionVektor();
	}

	private List<UpdateRepresentation> getAllUpdates() throws IOException, FileNotFoundException {
		File baseDir = new File(baseFolder);
		String[] updateFiles = baseDir.list();
		List<UpdateRepresentation> relevant = new LinkedList<>();
		for (String fileName : updateFiles) {
			relevant.add(new UpdateRepresentation(new File(baseFolder + "/" + fileName)));

		}
		return relevant;
	}

	private List<UpdateRepresentation> getAllUpdatesFor(String name) throws FileNotFoundException, IOException {
		List<UpdateRepresentation> all = getAllUpdates();
		return all.stream().filter(ur -> ur.getName().equals(name)).collect(Collectors.toList());
	}

	public List<UpdateRepresentation> getAllWaitingUpdates() throws FileNotFoundException, IOException {

		int[] version = getLatesUpdateVersion();
		List<UpdateRepresentation> allUpdates = getAllUpdates();
		java.util.Collections.sort(allUpdates);

		// as it is ordered we can drop the first few until we find some that are more
		// recent
		while (allUpdates.size() > 0)
			if (allUpdates.get(0).compare(version) < 0)
				allUpdates.remove(0);
			else
				break;
		return allUpdates;
	}

	public List<UpdateRepresentation> getAllWaitingUpdatesFor(String name)
			throws Exception {

		int[] version = getLatesVersionFor(name);
		List<UpdateRepresentation> allUpdates = getAllUpdatesFor(name);
		java.util.Collections.sort(allUpdates);

		// as it is ordered we can drop the first few until we find one that is more
		// recent
		while (allUpdates.size() > 0)
			if (allUpdates.get(0).compare(version) < 1)
				allUpdates.remove(0);
			else
				break;

		return allUpdates;
	}

	private void executeUpdate(UpdateRepresentation update) throws Exception {

		Connection c = connection.getConnection();

		connection.selectDatabase(new DatabaseSelector(update.getName()));

		for (String instruction : update.getInstructions()) {
			c.createStatement().execute(instruction);
		}

		Statement noteAction = c.createStatement();
		String tableName = version000.getCreateTableNames().get(0);

		connection.selectDatabase(versionDB);

		String noteUpdate = "INSERT INTO " + tableName + " (name,vX,vY,vZ) VALUES " + "('" + update.getName() + "',"
				+ update.getVersionVektor()[0] + "," + update.getVersionVektor()[1] + "," + update.getVersionVektor()[2]
				+ ");";
		noteAction.execute(noteUpdate);
	}

}