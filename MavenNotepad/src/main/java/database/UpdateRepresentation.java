package database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class UpdateRepresentation implements Comparable<UpdateRepresentation> {

	public static String commentStart = "-- ";

	public static String statementTermination = ";";

	private int[] versionVektor;
	private String name;

	private List<String> comments;
	private List<String> instructions;

	public static void main(String[] args) throws Exception {
		File f = new File("/home/nicholas/git/noteworthy/MavenNotepad/src/main/resources/database/notepadX_v0_0_0.dbe");
		UpdateRepresentation uR = null;
		try {
			uR = new UpdateRepresentation(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(uR.getName());
		for (int i = 0; i < uR.getVersionVektor().length; i++) {
			System.out.print(uR.getVersionVektor()[i]);
		}
		System.out.println();

		System.out.println(uR.getComments());

		for (String inst : uR.getInstructions())
			System.out.println(inst);
		// System.out.println(uR.getInstructions());
		// DatabaseConnectionHandler dbConnect = new
		// DatabaseConnectionHandler("/home/nicholas/git/noteworthy/MavenNotepad/src/main/webapp/resourcess/dbLoginData.txt");
		// Statement stm = dbConnect.getConnection().createStatement();
		// stm.execute("use X;");
		// ScriptRunner sr = new ScriptRunner(dbConnect.connection, false, true);
		// FileReader fr = new
		// FileReader("/home/nicholas/git/noteworthy/MavenNotepad/src/main/resources/database/notepadX_v0_0_0.dbe");
		// BufferedReader br = new BufferedReader(fr);
		//
		// sr.runScript(br);
		//
		// br.close();
		// fr.close();
	}

	public UpdateRepresentation(File file) throws IOException, FileNotFoundException {
		String fileName = file.getName();

		String[] parts = fileName.split("_");
		name = parts[0];

		parts[1] = parts[1].substring(1);// removing v
		parts[parts.length - 1] = parts[parts.length - 1].substring(0, parts[parts.length - 1].indexOf("."));// removing
																												// v

		versionVektor = new int[parts.length - 1];

		for (int i = 1; i < parts.length; i++)
			versionVektor[i - 1] = Integer.valueOf(parts[i]);

		processFile(file);

	}

	public UpdateRepresentation() {

	}

	private void processFile(File file) throws FileNotFoundException, IOException {
		comments = new LinkedList<String>();

		List<String> contentLines = new LinkedList<>();

		BufferedReader fileReader = new BufferedReader(new FileReader(file));
		while (fileReader.ready()) {
			String line = fileReader.readLine();

			if (line.startsWith(commentStart))
				comments.add(line.substring(commentStart.length()));
			else
				contentLines.add(line);
		}

		instructions = combineInstructions(contentLines);

		fileReader.close();
	}

	private List<String> combineInstructions(List<String> contentLines) {

		for (int i = 0; i < contentLines.size(); i++)
			contentLines.add(i, contentLines.remove(i).trim());

		for (int i = 0; i < contentLines.size() - 1;) {// reducing to only one empty line at a row
			if ("".equals(contentLines.get(i)) && "".equals(contentLines.get(i + 1)))
				contentLines.remove(i);
			else
				i++;
		}

		// combining lines to the full statements

		List<String> result = new LinkedList<>();
		StringBuilder currentStatement = null;

		for (String line : contentLines)
			if ("".equals(line))
				currentStatement = new StringBuilder();
			else if (line.endsWith(statementTermination))
				result.add(currentStatement.append(line).toString());
			else
				currentStatement.append(line);

		return result;
	}

	@Override
	public int compareTo(UpdateRepresentation o) {

		return compare(o.versionVektor);
	}

	public int compare(int[] vektor) {
		for (int i = 0; i < vektor.length; i++)
			if (versionVektor[i] < vektor[i])
				return -1;
			else if (versionVektor[i] > vektor[i])
				return 1;

		return 0;
	}

	public int[] getVersionVektor() {
		return versionVektor;
	}

	public List<String> getComments() {
		return comments;
	}

	public String getName() {
		return name;
	}

	public List<String> getInstructions() {
		return instructions;
	}

	public List<String> getCreateTableNames() {
		List<String> result = new LinkedList<String>();
		int start;
		for (String inst : getInstructions())
			if (inst.startsWith("CREATE TABLE"))
				result.add(inst.substring((start = inst.indexOf("`")) + 1, inst.indexOf("`", start + 1)));

		return result;
	}

	public void setVersionVector(int[] version) {
		this.versionVektor = version;
	}
}