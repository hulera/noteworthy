package database;

public class DatabaseSelector {
	
	private String databaseName;

	public DatabaseSelector(String databaseName) {
		setDatabaseName(databaseName);
	}
	
	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	

}
