package database;

public abstract class DatabaseModule {

	private DatabaseConnection connection;

	public DatabaseConnection getConnectionhandler() {
		return connection;
	}

	public void setConnection(DatabaseConnection connection) {
		this.connection = connection;
	}
}
