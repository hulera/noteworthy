package database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

public class DatabaseLogFileWriter {

	public static final String logFileName = "db_changelog.txt";
	
	private String logFileDir;

	
	public DatabaseLogFileWriter() {
		logFileDir = "src/main/webapp/resources/db_changelog.txt";
	}
	
	public DatabaseLogFileWriter(String textFilePathUpdate) {
		logFileDir = "src/main/webapp/resources/db_changelog.txt";
		
		try {
			addCommentsToLogFile(textFilePathUpdate);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void addCommentsToLogFile(String testFilePathUpdate) throws IOException {
		FileReader fr = new FileReader(testFilePathUpdate);
		BufferedReader br = new BufferedReader(fr);
		while (br.ready()) {
			System.out.println(br.readLine());
		}
		br.close();
		
	}
	
	public void addComments(String filePath) throws IOException {
		FileReader fr = new FileReader(filePath);
		BufferedReader br = new BufferedReader(fr);
		while (br.ready()) {
			System.out.println(br.readLine());
		}
		br.close();
	}

	public File getFile() throws IOException {
		File file = new File(logFileDir + "/" + logFileName);
		
		if(!file.exists())
			file.createNewFile();
		
		return file;
	}

	public String getLogFileDir() {
		return logFileDir;
	}

	public void setLogFileDir(String logFileDir) {
		this.logFileDir = logFileDir;
	}

}
